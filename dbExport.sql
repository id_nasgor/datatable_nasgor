-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2016 at 06:48 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_albertz`
--

-- --------------------------------------------------------

--
-- Table structure for table `motor`
--

CREATE TABLE IF NOT EXISTS `motor` (
`id` int(11) NOT NULL,
  `name` varchar(99) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor`
--

INSERT INTO `motor` (`id`, `name`) VALUES
(2, 'motor merah'),
(1, 'motor hijau');

-- --------------------------------------------------------

--
-- Table structure for table `motor_spek`
--

CREATE TABLE IF NOT EXISTS `motor_spek` (
`id` int(11) NOT NULL,
  `id_motor` int(11) NOT NULL,
  `spek` varchar(225) NOT NULL,
  `ket` varchar(225) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `motor_spek`
--

INSERT INTO `motor_spek` (`id`, `id_motor`, `spek`, `ket`) VALUES
(1, 1, 'jumlah cc', '150cc'),
(2, 1, 'warna', 'hijau'),
(3, 1, 'harga', '22000000'),
(4, 2, 'jumlah cc', '110cc'),
(5, 2, 'warna', 'merah'),
(6, 2, 'harga', '14000000');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `motor`
--
ALTER TABLE `motor`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motor_spek`
--
ALTER TABLE `motor_spek`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `motor`
--
ALTER TABLE `motor`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `motor_spek`
--
ALTER TABLE `motor_spek`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
