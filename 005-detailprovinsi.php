<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="css/jquery-editable.css">

  
<script type="text/javascript" charset="utf8" src="js/jquery-1.9.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/moment.min.js"></script>
<script type="text/javascript" charset="utf8" src="js/jqueryui-editable.js"></script> 

<div style='width:809px;margin:auto'>
	<h3>Provinsi</h3>
	<table id="province" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
				<th>ID</th>
				<th>KODE</th>
				<th>NAMA</th>
			</tr>
		</thead>
	</table>
</div>
 
<script>
$.fn.editable.defaults.mode = 'inline';
url="006-editProvince.php";
$(document).ready(function() {
    tableProvinsi=$('#province').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url":  "scripts/province2.php",
            "type": "POST"
        },
		"lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],
		"pageLength": 5,
		"drawCallback": function ( settings ) {
				provEditable();
            }
    } ); 
} );
function provEditable(){
	 $('.nama_prov').editable({
		clear:true,
		placeholder:"tuliskan nama provinsi",
		url:url
		
    });
	$('.code_prov').editable({
		clear:true,
		placeholder:"tuliskan Kode",
		url:url		
    });
}
</script>